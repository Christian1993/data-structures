export default class Node {
  constructor(data, next = null) {
    this.data = data;
    this.next = next;
  }
}

export class NodeDbl {
  constructor(data, next = null, previous = null) {
    this.data = data;
    this.next = next;
    this.previous = previous;
  }
}

export class GraphNode {
  constructor(vertex, neighbors = []) {
    this.vertex = vertex;
    this.neighbors = neighbors;
  }
  addNeighbor(vertex) {
    this.neighbors.push(vertex);
  }
}
