import {NodeDbl} from './Node';

export default class DoublyLinkedList {
  constructor(head = null, tail = null, length = 0) {
    this.head = head;
    this.tail = tail;
    this.length = length;
  }

  empty() {
    return this.length === 0;
  }

  push(data) {
    const node = new NodeDbl(data);
    if (this.head === null) {
      this.head = node;
      this.tail = node;
      this.length++;
      return node;
    } else {
      this.tail.next = node;
      node.previous = this.tail;
      this.tail = node;
      this.length++;
    }
  }

  pop() {
    if (this.empty()) {
      return null;
    }

    const node = this.tail;

    if (this.head === this.tail) {
      this.head = null;
      this.tail = null;
      this.length--;
      return node;
    }

    const penultimate = this.tail.previous;
    penultimate.next = null;
    this.tail = penultimate;

    this.length--;
    return node;
  }

  search(i) {
    if (i < 0 || i > this.length) {
      return null;
    } else if (i === 0) {
      return this.head;
    } else {
      let tmp = this.head;
      for (let j = 0; j < i; j++) {
        tmp = tmp.next;
      }
      return tmp;
    }
  }

  remove(i) {
    if (i === 0) {
      const tmp = this.head;
      this.head = tmp.next;
      this.length--;
      return tmp;
    } else if (i < 0 || i > this.length) {
      return null;
    }
    let tmp = this.head;
    for (let j = 0; j < i; j++) {
      tmp = tmp.next;
    }
    const previous = tmp.previous;
    const next = tmp.next;
    previous.next = next;
    next.previous = previous;

    this.length--;
    return tmp;
  }

  showAll() {
    let tmp = this.head;
    const list = [];
    while (tmp) {
      list.push(tmp.data);
      tmp = tmp.next;
    }
    return list;
  }
}
