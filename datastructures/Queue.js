import {validate} from '../decorators/decorators';

export default class Queue {
  constructor(queue = []) {
    this.queue = queue.reverse();
  }

  push(el) {
    this.queue.unshift(el);
  }

  @validate
  remove() {
    return this.queue.pop();
  }

  @validate
  front() {
    return this.queue[this.queue.length - 1];
  }

  back() {
    return this.queue[0];
  }

  empty() {
    return this.queue.length === 0;
  }

  size() {
    return this.queue.length;
  }

  showAll() {
    return this.queue;
  }
}
