import {GraphNode} from './Node';

export default class Graph {
  constructor(directed = true, vertices = [], edges = []) {
    this.directed = directed;
    this.vertices = vertices;
    this.edges = edges;
  }

  addVertex(vertex) {
    this.vertices.push(new GraphNode(vertex));
  }

  find(vertex) {
    return this.vertices.find(v => v.vertex === vertex);
  }

  addEdge(v1, v2) {
    const vertex1 = this.find(v1);
    const vertex2 = this.find(v2);
    vertex1.addNeighbor(v2);
    this.edges.push(`${v1}-${v2}`);
    if (!this.directed) {
      vertex2.addNeighbor(v1);
    }
  }

  showAll() {
    return this.vertices.map(({neighbors, vertex}) => {
      let output = `Node: ${vertex}`;
      if (neighbors.length) {
        output += ` Neighbors: ${neighbors.join(' ')}`
      }
      return output;
    }).join('\n');
  }
}
