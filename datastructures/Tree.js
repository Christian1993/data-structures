export default class Tree {
  constructor(data, parent = null, children = []) {
    this.data = data;
    this.parent = parent;
    this.children = children;
  }
  addChild(data) {
    const node = new Tree(data, this);
    node.parent = this;
    this.children.push(node);
    return node;
  }
  showChildren(node) {
    let output = '';
    const recurse = (node, level) => {
      output += '\t'.repeat(level);
      output += node.data;
      output += '\n';

      if (node.children.length) {
        node.children.forEach(child => recurse(child, level + 1));
      }
    };
    recurse(node, 0);
    return output;
  }
}
