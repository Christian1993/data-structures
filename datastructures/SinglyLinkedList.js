import Node from './Node';

export default class SinglyLinkedList {
  constructor(head = null, tail = null, length = 0) {
    this.head = head;
    this.tail = tail;
    this.length = length;
  }

  empty() {
    return this.length === 0;
  }

  push(data) {
    const node = new Node(data);
    if (this.head === null) {
      this.head = node;
      this.tail = node;
      this.length++;
      return node;
    } else {
      this.tail.next = node;
      this.tail = node;
      this.length++;
    }
  }

  pop() {
    if (this.empty()) {
      return null;
    }
    const node = this.tail;
    if (this.head === this.tail) {
      this.head = null;
      this.tail = null;
      this.length--;
      return node;
    }

    let tmp = this.head;
    let penultimate = null;

    while (tmp) {
      if (tmp.next === this.tail) {
        penultimate = tmp;
        break;
      }
      tmp = tmp.next;
    }
    penultimate.next = null;
    this.tail = penultimate;
    this.length--;
    return node;
  }

  search(i) {
    if (i < 0 || i > this.length) {
      return null;
    } else if (i === 0) {
      return this.head;
    } else {
      let tmp = this.head;
      for (let j = 0; j < i; j++) {
        tmp = tmp.next;
      }
      return tmp;
    }
  }

  remove(i) {
    if (i < 0 || i > this.length) {
      return null;
    }
    else if (i === 0) {
      const tmp = this.head;
      this.head = tmp.next;
      this.length--;
      return tmp;
    }
    let tmp = this.head;
    let prev = null;
    for (let j = 0; j < i; j++) {
      prev = tmp;
      tmp = tmp.next;
    }
    prev.next = tmp.next;
    this.length--;
    return tmp;
  }

  showAll() {
    let tmp = this.head;
    const list = [];
    while (tmp) {
      list.push(tmp.data);
      tmp = tmp.next;
    }
    return list;
  }
}
