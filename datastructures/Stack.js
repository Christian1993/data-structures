import {validate} from '../decorators/decorators';

export default class Stack {
  constructor(stack = []) {
    this.stack = stack;
  }

  push(el) {
    this.stack.push(el);
  }

  @validate
  pop() {
    return this.stack.pop();
  }

  @validate
  top() {
    return this.stack[this.stack.length - 1];
  }

  bottom() {
    return this.stack[0];
  }

  empty() {
    return this.stack.length === 0;
  }

  size() {
    return this.stack.length;
  }

  showAll() {
    return this.stack;
  }
}
