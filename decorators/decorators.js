export const validate = (target, name, descriptor) => {
  const func = descriptor.value;
  descriptor.value = function () {
    const className = target.constructor.name;
    let length = null;

    switch (className) {
      case 'Queue': {
        length = this.queue.length;
        break;
      }
      case 'Stack': {
        length = this.stack.length;
        break;
      }
      default:
        return;
    }

    if (length > 0) {
      return func.apply(this);
    } else {
      return null;
    }
  };
  return descriptor;
};
