import Queue from './datastructures/Queue';
import Stack from './datastructures/Stack';
import SinglyLinkedList from './datastructures/SinglyLinkedList';
import DoublyLinkedList from './datastructures/DoublyLinkedList';
import Graph from './datastructures/Graph';
import Tree from './datastructures/Tree';

// Queue
console.log('-----Queue-----');
const heroes = new Queue();

heroes.push('Batman');
heroes.push('Superman');

console.log('All elements: ' + heroes.showAll()); // ['Superman', 'Batman']
console.log('Is empty: ' + heroes.empty()); // false
console.log('Size: ' +  heroes.size()); // 2

console.log('Front: ' + heroes.front()); // Batman
console.log('Back: ' + heroes.back()); // Superman

console.log('Remove: ' + heroes.remove()); // Batman
console.log('All elements: ' + heroes.showAll()); // ['Superman']

// Stack
console.log('\n', '-----Stack-----');
const heroes2 = new Stack();

heroes2.push('Batman');
heroes2.push('Superman');
heroes2.push('Thor');
console.log('All elements: ' + heroes2.showAll()); // ['Batman', 'Superman', 'Thor']

console.log('Is empty: ' + heroes2.empty()); // false
console.log('Size: ' + heroes2.size()); // 3
console.log('Top: ' + heroes2.top()); // Thor
console.log('Bottom: ' + heroes2.bottom()); // Batman

console.log('Pop element: ' + heroes2.pop()); // Thor
console.log('All elements: ' + heroes2.showAll()); // ['Batman', 'Superman']

// Singly Linked List
console.log('\n', '-----Singly linked list-----');

const list = new SinglyLinkedList();
list.push('a');
list.push('b');
list.push('c');
list.push('d');

console.log('Element 0: ', list.search(0)); // Node a
console.log('Element 1: ', list.search(1).data); // b
console.log('Element 7: ', list.search(7)); // null

console.log('Pop element: ', list.pop()); // Node d
console.log('Remove element 1: ', list.remove(1)); // Node b
console.log('All elements: ', ...list.showAll()); // a c

// Doubly Linked List
console.log('\n', '-----Doubly linked list-----');

const gms = new DoublyLinkedList();
gms.push('Garry Kasparov');
gms.push('Magnus Carlsen');
gms.push('Mikhail Tal');

console.log('Element 0: ', gms.search(0)); // Node Garry Kasparov
console.log('Element 1: ', gms.search(1).data); // Magnus Carlsen
console.log('Element 7: ', gms.search(7)); // null

console.log('Remove element 1: ', gms.remove(1)); // Node Magnus Carlsen
console.log('All elements: ', ...gms.showAll()); // Garry Kasparov Mikhail Tal

// Graph
console.log('\n', '-----Graph-----');

const graph = new Graph();
graph.addVertex('a');
graph.addVertex('b');
graph.addVertex('c');
graph.addVertex('d');


graph.addEdge('a', 'b'); // a -> b
graph.addEdge('a', 'c'); // a -> c -> d
graph.addEdge('c', 'd');

console.log(graph.showAll());

// Tree
console.log('\n', '-----Tree-----');

const root = new Tree('Batman');
const jl = root.addChild('Justice League');
const yj = root.addChild('Young Justice');

jl.addChild('Superman');
jl.addChild('Cyborg');
jl.addChild('Flash');

yj.addChild('Superboy');
yj.addChild('Impulse');

console.log(root.showChildren(root));
